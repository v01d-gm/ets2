import threading
import time

import keyboard

import ets2telemetry.sharedmemory
import ets2telemetry.ets2telemetry


script_activated = False
ets2memory = ets2telemetry.sharedmemory.SharedMemory()


def get_gamestate():
    return ets2telemetry.ets2telemetry.Ets2Telemetry(ets2memory.update())


def on_insert_press(event):
    global script_activated
    script_activated = not script_activated
    print(f"{script_activated=}")


def press_gas(delay: float, button_to_press: str = "w"):
        keyboard.press(button_to_press)
        time.sleep(delay)
        keyboard.release(button_to_press)
        time.sleep(delay)


def delay(speed: float):
    b = 0.3
    a = (0.07 - b) / 10
    return max(a * speed + b, 0.005)


def main():
    keyboard.on_press_key("Insert", on_insert_press)
    print("Press Insert to activate")

    while True:
        if not script_activated:
            continue

        gamestate = get_gamestate()

        if (
            not gamestate.DriveTrain.EngineEnabled
            or gamestate.DriveTrain.MotorBrake
            or gamestate.DriveTrain.ParkingBrake
            or gamestate.Physics.SpeedKmh > 10
        ):
            continue

        press_gas(0.0001)

        if gamestate.DriveTrain.Gear == 0:
            time.sleep(0.5)
            continue

        gas_factor = 0.2 if gamestate.DriveTrain.Gear > 100 else 1
        press_gas(delay(gamestate.Physics.SpeedKmh) * gas_factor)


if __name__ == "__main__":
    main()
